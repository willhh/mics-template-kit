<?php

// No comment    <-- LOL! LOOK! A JOKE :D

function dbConnect() {    // This function accepts a query then returns a result.
                                // It handles all the database conections as well
    // Database config
    $host = "localhost";
    $user = "root";
    $password = "";
    $database = "serverassthree";
    // Connects to database and runs query. returns result.
    $connection = mysqli_connect($host, $user, $password, $database);
    return;
}

function query() {
    $result = mysqli_query($connection, $query);
}

function getName() {    // Function Gets users full name from database
    // Your database column names for users first name and surname
    // Assumes first and surnames stored in separete columns
    $firstNameColumn    =   'name';
    $surnameColumn      =   'surname';
    
    // Databasey stuffs
    $user = $_SESSION['username']; // username must be stored in session
    $query = "SELECT * FROM users WHERE username='$user'";
    $data = mysqli_fetch_assoc(dbConnect($query));
    $name = $data[$firstNameColumn] . " " . $data[$surnameColumn];
    return $name;
}

function isAdmin() {    // Checks users rank to see if they're admin
    // Returns true or false as string
    // Set admin rank number
    
    $adminRankNum   =   '1';
    
    // Databasey stuffs
    $user = $_SESSION['username'];  // Username stored in session
    $query = "SELECT * FROM users WHERE username='$user'";
    $data = mysqli_fetch_assoc(dbConnect($query));
    if ($data['admin'] === $adminRankNum) {
        $admin = 'true';
    } else {
        $admin = 'false';
    }
    return $admin;
}

function getUserImages($username) {
    
    $userDirectory      =   'userdir';      // The name of the column that the users directory information is stored in
    $uploadDirectoy     =   'userImages/files/';    // The path to the images directory your user directories are stored in
    $imageFail          =   $uploadDirectoy . 'fail.png';   // If there is no images then it will display this file
    
    // Databasey stuff and creating a directory handler etc.
    $query = "SELECT * FROM users WHERE username='$username'";
    $data = mysqli_fetch_assoc(dbConnect($query));
    $dir = $uploadDirectoy . $data[$userDirectory] . "/"; 
    $dh = opendir($dir);

    // This part sees if there is any files in a directory

    if (($files = @scandir($dir)) && (count($files) > 3)) {
        $directory_not_empty = 'true';
    } else {
        $directory_not_empty = 'false';
    }

    if ($directory_not_empty == 'true') { // If there is images in the folder then do stuff :D

        while ($filename = readdir($dh)) {  // Opens directory
            $filepath = $dir . $filename;
            if (is_file($filepath) and preg_match("~\.jpg|JPG|jpeg|JPEG|PNG|png|gif$~", $filename)) {   //Checks to see if files are images
                $gallery[] = $filepath; //puts image into array
            }
        }

        foreach ($gallery as $image) { //Spits out images (WOOOO!!!!) :D
            echo "<img src='$image' longdesc='$image' width='1024' height='768' alt='' />\n";
        }
    } else {
        echo "<img src='$imageFail' longdesc='$imageFail' width='1024' height='768' alt='' />\n";
        echo "<img src='$imageFail' longdesc='$imageFail' width='1024' height='768' alt='' />\n";
        echo "<img src='$imageFail' longdesc='$imageFail' width='1024' height='768' alt='' />\n";
    }
}

function logout() { // I'm sure you can work out what this one does for yourself...
    session_destroy();
    header('Location: index.php');
}

function clientInfo() { // Get browser, OS, and other data etc. and put in an array
    $info = browser_detection('full_assoc'); // see detect.php for a crap ton of comments
    
    // Woo if statements... work out what it does yourself if you want to fiddle with this
    // you'll want to look at detect.php
    if($info['browser_working'] == 'moz'){
        $browser = $info['moz_data'][0] . " version " . $info['moz_data'][3];
    } elseif ($info['browser_working'] == 'webkit') {
        $browser = $info['webkit_data'][0] . " version " . $info['webkit_data'][1];
    } else {
        $browser = $info['browser_working'] . " version " . $info['browser_number'];
    }
    
    $clientInfo = array(
        'browser'   =>  $browser,
        'os'        =>  strtoupper($info['os']) . " " . $info['os_number'],
        'full'      =>  $_SERVER['HTTP_USER_AGENT']
    );
    return $clientInfo;
}

?>
